# ics-ans-role-basic-packages

Ansible role to install basic-packages.

## Role Variables

```yaml
basic_packages_list is a list of packages that should be installed on all centos hosts

basic_packages_list_pysical_machines is a list of packages that should be installed on all centos physical hosts (csentry_device_type == "PhysicalMachine")
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-basic-packages
```

## License

BSD 2-clause
